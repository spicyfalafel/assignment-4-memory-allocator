#define _DEFAULT_SOURCE

#include <assert.h>
#include <unistd.h>

#include "mem.h"
#include "test.h"
#include "util.h"

#define INITIAL_HEAP_SIZE 10240

static void *test_heap_init() {
  debug("Heap initializing started\n");
  void *heap = heap_init(INITIAL_HEAP_SIZE);
  if (heap == NULL) {
    err_red("ERR Cannot initialize heap");
  }
  debug_green("OK Heap initialized\n\n");
  return heap;
}

static inline struct block_header *get_block_by_allocated_data(void *data) {
  return (struct block_header *)((uint8_t *)data -
                                 offsetof(struct block_header, contents));
}

static void allocate_pages_after_block(struct block_header *last_block) {
  struct block_header *addr = last_block;
  void *test_addr = (uint8_t *)addr + size_from_capacity(addr->capacity).bytes;
  const size_t TEST_ADDR_LENGTH = 1024;
  test_addr = mmap(test_addr, TEST_ADDR_LENGTH, PROT_READ | PROT_WRITE,
      MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);
  debug(test_addr);
}

// Обычное успешное выделение памяти
void test1_normal_memory_allocation(struct block_header *first_block) {
  debug("Test 1 started:\n");

  const size_t test_size = 512;
  void *data1 = _malloc(test_size);
  if (data1 == NULL) {
    err_red("ERR Test 1 failed: _malloc returned NULL\n");
  }

  debug_heap(stdout, first_block);

  if (first_block->is_free) {
    err_red("ERR Test 1 failed: allocated_block isn't free\n");
  }
  if (first_block->capacity.bytes != test_size) {
    err_red("ERR Test 1 failed: allocated_block size isn't correct\n");
  }
  debug_green("OK Test 1 passed\n\n");

  _free(data1);
}

// Освобождение одного блока из нескольких выделенных
void test2_free_block(struct block_header *first_block) {
  debug("Test 2 started:\n");

  void *data1 = _malloc(512), *data2 = _malloc(600);
  if (data1 == NULL || data2 == NULL) {
    err_red("ERR Test 2 failed: _malloc returned NULL\n");
  }
  _free(data1);
  debug_heap(stdout, first_block);
  struct block_header *data_block1 = get_block_by_allocated_data(data1),
                      *data_block2 = get_block_by_allocated_data(data2);
  if (!data_block1->is_free) {
    err_red("ERR Test 2 failed: free block is taken\n");
  }
  if (data_block2->is_free) {
    err_red("ERR Test 2 failed: taken block is free\n");
  }

  debug_green("OK Test 2 passed\n\n");
  _free(data1);
  _free(data2);
}

// Освобождение двух блоков из нескольких выделенных
void test3_free_two_blocks(struct block_header *first_block) {
  debug("Test 3 started:\n");

  const size_t s1 = 512, s2 = 1024, s3 = 2048;
  void *data1 = _malloc(s1), *data2 = _malloc(s2), *data3 = _malloc(s3);
  if (data1 == NULL || data2 == NULL || data3 == NULL) {
    err_red("ERR Test 3 failed: _malloc returned NULL\n");
  }
  // освобождаем два блока подряд, а потом проверяем, объединились ли они
  _free(data2);
  _free(data1);
  debug_heap(stdout, first_block);
  struct block_header *data_block1 = get_block_by_allocated_data(data1),
                      *data_block3 = get_block_by_allocated_data(data3);
  if (!data_block1->is_free) {
    err_red("ERR Test 3 failed: free block is taken\n");
  }
  if (data_block3->is_free) {
    err_red("ERR Test 3 failed: taken block is free\n");
  }
  if (data_block1->capacity.bytes !=
      s1 + s2 + offsetof(struct block_header, contents)) {
    err_red("ERR Test 3 failed: two free blocks didn't connect\n");
  }
  debug_green("OK Test 3 passed\n\n");

  _free(data1);
  _free(data2);
  _free(data3);
}

// Память закончилась, новый регион памяти расширяет старый
void test4_new_region_after_last(struct block_header *first_block) {
  debug("Test 4 started:\n");

  void *data1 = _malloc(INITIAL_HEAP_SIZE),
       *data2 = _malloc(INITIAL_HEAP_SIZE + 512), *data3 = _malloc(2048);
  if (data1 == NULL || data2 == NULL || data3 == NULL) {
    err_red("ERR Test 4 failed: _malloc returned NULL\n");
  }
  _free(data3);
  _free(data2);

  debug_heap(stdout, first_block);

  struct block_header *data_block1 = get_block_by_allocated_data(data1),
                      *data_block2 = get_block_by_allocated_data(data2);

  if ((uint8_t *)data_block1->contents + data_block1->capacity.bytes !=
      (uint8_t *)data_block2) {
    err_red("ERR Test 4 failed: new region wasn't created after last\n");
  }
  debug_green("OK Test 4 passed\n\n");

  _free(data1);
  _free(data2);
  _free(data3);
}

// Память закончилась, старый регион памяти не расширить из-за другого
// выделенного диапазона адресов, новый регион выделяется в другом месте.
void test5_new_region_at_new_place(struct block_header *first_block) {
  debug("Test 5 started:\n");

  void *data1 = _malloc(10000);
  if (data1 == NULL) {
    err_red("ERR Test 5 failed: _malloc returned NULL\n");
  }

  struct block_header *addr = first_block;
  while (addr->next != NULL)
    addr = addr->next;
  allocate_pages_after_block(addr);
  void *data2 = _malloc(100000);

  debug_heap(stdout, first_block);

  struct block_header *data2_block = get_block_by_allocated_data(data2);
  if (data2_block == addr) {
    err_red("ERR Test 5 failed: new block wasn't allocated at new place\n");
  }
  debug_green("OK Test 5 passed\n\n");

  _free(data1);
  _free(data2);
}

void test() {
  struct block_header *first_block = (struct block_header *)test_heap_init();
  test1_normal_memory_allocation(first_block);
  test2_free_block(first_block);
  test3_free_two_blocks(first_block);
  test4_new_region_after_last(first_block);
  test5_new_region_at_new_place(first_block);
  debug_green("OK: All tests passed\n\n");
}
