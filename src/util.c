#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"
#define CLR_RED "\033[1;31m"
#define CLR_GREEN "\033[0;32m"
#define CLR_REGULAR "\033[0m"

_Noreturn void err(const char *msg, ...) {
  va_list args;
  va_start(args, msg);
  vfprintf(stderr, msg, args); // NOLINT
  va_end(args);
  abort();
}


void debug_green(const char *msg) {
  debug(CLR_GREEN);
  debug(msg);
  debug(CLR_REGULAR);
}

void err_red(const char *msg) {
  debug(CLR_RED);
  err(msg);
  debug(CLR_REGULAR);
}

extern inline size_t size_max(size_t x, size_t y);
